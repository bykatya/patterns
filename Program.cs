using System;
using System.Collections.Generic;

namespace pattern_pr2
{
    class Program
    {
        static void Main(string[] args)
        {
            // список танков   
            var _tanks = new List<ITank>();
            // начало игры
            var game = new TankGame(_tanks);
            game.Play();
        }
        public class Tank
        {
            public int Health { get; set; }
            public int Damage { get; set; }
            public string Color { get; set; }
            // создает копию танка
            public Tank Clone()
            { 
                return (Tank)this.MemberwiseClone();
            }
        }

        // целевой интерфейс с методами для управления танком  
        public interface ITank
        {
            void MoveForward();
            void MoveBackward();
            void Turn();
            void Fire();
        }

        // для управления танками через интерфейс 
        public class TankAdapter : ITank
        {
            private Tank _tank;
            public TankAdapter(Tank tank)
            {
                _tank = tank;
            }
            // метод движения танка  
            public void MoveForward()
            {
                Console.WriteLine($"Танк {_tank.Color} движется вперед");
            }
            // метод движения танка  
            public void MoveBackward()
            {
                Console.WriteLine($"Танк {_tank.Color} движется назад");
            }
            // метод поворота танка 
            public void Turn()
            {
                Console.WriteLine($"Танк {_tank.Color} поворачивается");
            }
            // метод выстрела танка 
            public void Fire()
            {
                Console.WriteLine($"Танк {_tank.Color} горит с уроном {_tank.Damage}");
            }
        }

        public abstract class Game
        {
            protected abstract void Initialize();
            protected abstract void Start();
            protected abstract void Update();
            protected abstract void End();
            public void Play()
            {
                Initialize();
                Start();
                Update();
                IsGameOver();
                End();
            }
            protected virtual bool IsGameOver()
            {
                return false;
            }
        }

        // адаптируемый класс
        public class TankGame : Game
        {
            private List<ITank> _tanks;
            public TankGame(List<ITank> tanks)
            {
                _tanks = tanks;
            }
            //использование объектов класса TankAdapter в игре TankGame
            protected override void Initialize()
            { 
                // создание объектов-прототипов 
                var tank1 = new Tank { Health = 100, Damage = 10, Color = "желтый" };
                var adapter1 = new TankAdapter(tank1);

                var tank2 = new Tank { Health = 120, Damage = 15, Color = "красный" };
                var adapter2 = new TankAdapter(tank2);

                var newTank1 = tank1.Clone();
                newTank1.Color = "синий";
                newTank1.Damage = 12;
                var newTank2 = tank2.Clone();
                newTank2.Color = "зеленый";
                newTank2.Health = 150;

                // управление танками
                adapter1.MoveForward();
                adapter1.Turn();
                adapter1.Fire();

                adapter2.MoveBackward();
                adapter2.Turn();
                adapter2.Fire();

                _tanks.Add(new TankAdapter(newTank1));
                _tanks.Add(new TankAdapter(newTank2));
            }
            // начало игры
            protected override void Start()
            {
                Console.WriteLine("Игра началась!");
            }
            // обновление игры
            protected override void Update()
            {
                foreach (var tank in _tanks)
                {
                    tank.MoveForward();
                    tank.MoveBackward();
                    tank.Turn();
                    tank.Fire();
                }
            }
            // конец игры
            protected override void End()
            {
                Console.WriteLine("Игра закончилась!");
            }
            // проверка условий окончания игры
            protected override bool IsGameOver()
            { 
                return false;
            }
        }
    }
}
